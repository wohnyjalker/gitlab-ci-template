Gitlab-CI Docker Template
==================================================
Simple template to build, test and deploy your project.
### gitlab-ci.yml
Informations about predefined variables used in yml file:
* $CI_REGISTRY_USER - Your user account name
* $CI_BUILD_TOKEN - Gitlab acts as an authentication provider for the registry.CI_BUILD_TOKEN is set automatically for each build by Gitlab-CI
* $CI_REGISTRY  - Docker registry
* $CI_REGISTRY_IMAGE - `Gitlab acts as an authentication provider for the registry(...) CI_BUILD_TOKEN is set automatically for each build by Gitlab-CI by Martin stackoverflow.com`
* $CI_COMMIT_SHA
  
More about [Gitlab-CI predefined variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)

Variables set in Settings/CI/CD/Variables:
* $REMOTE_USER - Remote server user
* $REMOTE_SERVER - Remote host
* $REMOTE_COMMAND - Script to execute on remote server
* $SSH_PRIVATE_KEY - Remote user private rsa key  

More about [SSH](https://phoenixnap.com/kb/ssh-to-connect-to-remote-server-linux-or-windows)  
  
Where to find built images:
* gitlab.com/< username >/< repository name >/container_registry

### update.sh
If you have multiple containers running on your remote host you can easily filter them to stop only desire one
```
docker container stop $(docker container ls -q --filter name=it_academy)
```

