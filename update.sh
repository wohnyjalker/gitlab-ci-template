#!/bin/sh

# color variables
RED='\033[0;31m'
GREEN='\033[032m'
NC='\033[0m'

echo "${0}: Trying to stop containers..."
if docker container stop $(docker container ls -q --filter name=it_academy); then
	echo "${0}: ${GREEN}Command succeeded${NC}"
else
	echo "${0}: ${RED}Command failed${NC}"
	# exit 1
fi

echo "${0}: Pulling changes..."

cd /home/docker/projects/django_slack_storage/src 

if git pull; then
	echo "${0}: ${GREEN}Sucessfully pulled from repository.${NC}"
else
	echo "${0}: ${RED}Error when updating local repository.${NC}"
fi

echo "${0}: Running docker..."

if ./rundocker.sh; then
	echo "${0}: ${GREEN}Docker is running...${NC}"
else
	echo "${0}: ${RED}Error while running rundocker.sh${NC}"
	exit 1
fi

